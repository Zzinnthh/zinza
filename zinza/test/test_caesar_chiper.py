from mycode.ceasar_cipher_util import caesarCipher

import unittest

class chatzinzea(unittest.TestCase):
    def test_give_abcd_is_string(self):
        zin = 'abcd'
        num = 2

        expected_output = 'cdef'
        
        result = caesarCipher(zin,num)
        self.assertEqual(result,expected_output)